#pragma once

#include "meshnode.h"
#include <vector>
#include <map>
#include <string>
#include <set>

class MeshGraph {
public:
	class Vertex : public MeshNode {
	public:
		float color[3];

	public:
		Vertex() {
			color[0] = 0;
			color[1] = 0;
			color[2] = 0;
		}
		~Vertex() {}
	};	

protected:
	std::vector<MeshGraph::Vertex> vertices;
	std::map<size_t, std::set<size_t> > edges;
	std::set<size_t> empty_set;

protected:
	void readPoints(const std::string& points);
	void readTriangles(const std::string& triangles);

public:
	MeshGraph() {}
	~MeshGraph() {}

	const std::vector<MeshGraph::Vertex>& getVertices() const {
		return vertices;
	}

	const std::set<size_t>& getEdges(const size_t& vertex_id) const {
		return (edges.find(vertex_id) != edges.end()) ? edges.at(vertex_id) : empty_set;
	}

	void load(const std::string& points, const std::string& triangles);
	void exportInGraphML(const std::string& filename);
};
