#include "meshgraph.h"
#include "meshtriangle.h"
#include <iostream>
#include <fstream>
#include <boost/foreach.hpp>

void MeshGraph::load(const std::string& points, const std::string& triangles) {
	readPoints(points);
	readTriangles(triangles);
}

void MeshGraph::exportInGraphML(const std::string& filename) {
	std::ofstream f_out(filename.c_str());

	f_out << "<?xml version=\"1.0\" encoding=\"UTF-8\"?>" << std::endl;
	f_out << "<graphml xmlns=\"http://graphml.graphdrawing.org/xmlns\" xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\"\n\
		xsi:schemaLocation=\"http://graphml.graphdrawing.org/xmlns http://graphml.graphdrawing.org/xmlns/1.0/graphml.xsd\">" << std::endl;
	f_out << "<graph edgedefault=\"undirected\">" << std::endl;

	BOOST_FOREACH(const MeshGraph::Vertex& v, vertices) {
		f_out << "<node id=\"" << v.id << "\">\n";
		f_out << "</node>\n";
		BOOST_FOREACH(const size_t& target, edges[v.id]) {
			f_out << "<edge source=\"" << v.id << "\" target=\"" << target << "\">\n";
			f_out << "</edge>\n";
		}
	}	

	f_out << "</graph>\n</graphml>" << std::endl;
	f_out.close();
}

void MeshGraph::readPoints(const std::string& points) {
	std::ifstream f_in(points.c_str());
	if(!f_in.is_open()) {
		std::cerr << "Error: can't open " << points << std::endl;
		return;
	}

	size_t num_of_points;
		f_in >> num_of_points;
	vertices.resize(num_of_points);
	BOOST_FOREACH(Vertex& v, vertices) {
		f_in >> v.id >> v.lambda >> v.phi >> v.depth; // read vertex info
	}

	f_in.close();
}

void MeshGraph::readTriangles(const std::string& triangles) {
	std::ifstream f_in(triangles.c_str());
	if(!f_in.is_open()) {
		std::cerr << "Error: can't open " << triangles << std::endl;
		return;
	}

	size_t num_of_triangles;
		f_in >> num_of_triangles; 
	MeshTriangle triangle;
	for (size_t i = 0; i < num_of_triangles; i++) {		
		f_in >> triangle.id >> triangle.n[0] >> triangle.n[1] >> triangle.n[2] >>
			triangle.edge[0] >> triangle.edge[1] >> triangle.edge[2]; // read triangle info
		edges[triangle.n[0]].insert(triangle.n[1]); // create edges
		edges[triangle.n[1]].insert(triangle.n[0]);
		edges[triangle.n[1]].insert(triangle.n[2]);
		edges[triangle.n[2]].insert(triangle.n[1]);
		edges[triangle.n[2]].insert(triangle.n[0]);
		edges[triangle.n[0]].insert(triangle.n[2]);
	}
	f_in.close();
}

