#include "meshgraph.h"

int main(int argc, char **argv) {
	MeshGraph graph;

	graph.load("OKH_pts.dat", "OKH_tr.dat");
	graph.exportInGraphML("okh.graphml");
	return 0;
}
