#pragma once

class MeshPartitioner {
public:
	virtual void partition() = 0;
};